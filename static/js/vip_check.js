function updateInstance(instance_state, instance_element, service) {
    var service_element = $('#' + service);
    switch (instance_state) {
        case "MASTER":
            instance_element.removeClass();
            instance_element.addClass("table-active");
            instance_element.find("td").removeClass();
            if ( service_element.has(".table-danger").length <= 0 ) {
                if ( service_element.find(".table-active").length > 1 ) {
                    service_element.removeClass("border-success");
                    service_element.addClass("border-danger");
                } else {
                    service_element.removeClass("border-danger");
                    service_element.addClass("border-success");
                }
            }
            break;
        case "BACKUP":
            instance_element.removeClass();
            instance_element.find("td").removeClass();
            if ( service_element.has(".table-danger").length <= 0 ) {
                if ( service_element.has(".table-active").length <= 0 ) {
                    service_element.removeClass("border-success");
                    service_element.addClass("border-danger");
                } else {
                    service_element.removeClass("border-danger");
                    service_element.addClass("border-success");
                }
            }
            break;
        case "FAULT":
            if ( ! instance_element.hasClass("table-danger") ) {
                instance_element.removeClass();
                instance_element.addClass("table-danger");
                instance_element.find("td").addClass("cell-danger");
                $.playSound("/static/sounds/warning.wav");
            }
            service_element.removeClass("border-success");
            service_element.addClass("border-danger");
            break;
    }
}

function updateService(service, service_name) {
    for (var instance in service) {
        var instance_id = service[instance]['id'];
        var instance_ip = service[instance]['ip'];
        var instance_state = service[instance]['state'];
        var instance_container = $('#' + service_name + '> .card-body > table > tbody');

        if ( instance_container.find('#' + instance_id).length > 0 ) {
            // if the instance is already available inside this service
            updateInstance(instance_state, instance_container.find('#' + instance_id), service_name);
        } else {
            // if the instance is not available inside this service
            instance_container.append("<tr id='" + instance_id + "'><td>" + instance_ip + "</td></tr>");
            updateInstance(instance_state, instance_container.find('#' + instance_id), service_name);
        }
    }
}

$(document).ready(function() {
    $('#status_container').preloader({ text: 'Loading ...' });
});

var socket = new WebSocket('ws://' + document.domain + ':' + location.port + '/ws');

socket.onopen = function (event) {
    socket.send("init");
    $('#status_container').preloader('remove');
};

socket.onclose = function (p1) {
    console.log("Disconnected from " + p1['target']['url']);
    console.log(p1);
    $("#errorModal").modal({
        backdrop: 'static',
        keyboard: false
    })
};

socket.onerror = function (event) {
    console.log(event.data);
};

socket.addEventListener('message', function (event) {
    var response = JSON.parse(event.data);
    for (var service in response) {
        if ( $( "#status_container" ).has( '#' + service ).length > 0 ) {
            // if the service is already there on the page
            updateService(response[service], service);
        } else {
            // if the service is not on the page
            $( "#status_container" ).append("<div class='col-sm'>\n" +
                "                <div class='card text-white bg-dark mb-3' id='" + service + "'>\n" +
                "                    <div class='card-header'>" + service + " </div>\n" +
                "                    <div class='card-body'>\n" +
                "                        <table class='table table'>\n" +
                "                            <tbody>\n" +
                "                            </tbody>\n" +
                "                        </table>\n" +
                "                    </div>\n" +
                "                </div>\n" +
                "            </div>");
            updateService(response[service], service);
        }
    }
});