#!/bin/bash

## Enter MYIP manually if the server has multiple interfaces
MYIP=$(ip add show | grep -w inet | grep -vw lo | grep -vw secondary | awk '{ print $2 }' | cut -d/ -f 1)

URL="http://vipstatus.example.com"
SERVICE=$2
STATE=$3

/bin/curl "$URL/set?ip=$MYIP&service=$SERVICE&state=$STATE"
