from quart import Quart, request, Response, render_template, jsonify, websocket
import sqlite3
import json
import time
import os

db_file = os.path.join(os.getcwd(), 'data.db')

app = Quart(__name__)


@app.route('/init_db')
async def init_db():
    db_conn = sqlite3.connect(db_file)
    c = db_conn.cursor()
    c.execute('''CREATE TABLE vip_status 
              (server_ip text, service text, state text)''')
    db_conn.commit()
    db_conn.close()
    return "Done"


@app.route('/set', methods=['GET'])
def set_data():
    query_params = request.args
    db_conn = sqlite3.connect(db_file)
    c = db_conn.cursor()
    t = (query_params['ip'], query_params['service'])
    c.execute('SELECT * FROM vip_status WHERE server_ip=? AND service=?', t)
    result = c.fetchall()
    t = (query_params['state'],) + t
    if not result:
        print('nothing')
        c.execute('INSERT INTO vip_status (state, server_ip, service) VALUES (?, ?, ?)', t)
    else:
        c.execute('UPDATE vip_status SET state=? WHERE server_ip=? AND service=?', t)
    if c.rowcount > 0:
        status = {'Status': 'Successful'}
    else:
        status = {'Status': 'Failed'}

    db_conn.commit()
    db_conn.close()
    return jsonify(status)


@app.route('/get', methods=['GET'])
def get_data():
    vip_info = {}
    db_conn = sqlite3.connect(db_file)
    c = db_conn.cursor()
    c.execute('SELECT service,oid, server_ip, state FROM vip_status ORDER BY service, server_ip')
    result = c.fetchall()
    for row in result:
        if row[0] not in vip_info:
            vip_info[row[0]] = []
        vip_info[row[0]].append({'id': row[1], 'ip': row[2], 'state': row[3]})

    db_conn.close()
    return jsonify(vip_info)


@app.route('/')
async def index():
    return await render_template('index.html')


@app.websocket('/ws')
async def ws():
    while True:
        vip_info = {}
        db_conn = sqlite3.connect(db_file)
        c = db_conn.cursor()
        c.execute('SELECT service, oid, server_ip, state FROM vip_status ORDER BY service, server_ip')
        result = c.fetchall()
        for row in result:
            if row[0] not in vip_info:
                vip_info[row[0]] = []
            vip_info[row[0]].append({'id': row[1], 'ip': row[2], 'state': row[3]})

        db_conn.close()

        await websocket.send(f"{json.dumps(vip_info)}")
        time.sleep(5)


if __name__ == '__main__':
    app.run()
