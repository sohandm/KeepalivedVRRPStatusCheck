# Keepalived VRRP Status Check 

This program allows monitoring state changes of Keepalived VRRP instances and display on a web interface.


![Screenshot](https://s15.postimg.cc/56mpxobwr/VIPStatus.gif)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
Python 3.5 or above
Quart 0.5.0
uvloop 0.10.1
gunicorn 19.8.1 or an equivalent of your choice
```

### Installing

A step by step series of examples that tell you how to get a development env running

Prepare the environment

```
Install required python libraries
Clone the repository
Modify bind IP and port in gunicorn.py
```

Run the application using gunicorn

```
gunicorn --config gunicorn.py VIPStatus:app
```

Collect data from Keepalived VRRP instances
```
Use notify.sh as the notify script in all VRRP instances you need to monitor
Make sure to change the URL in notify script to access web app hosted with gunicorn above
```

## Deployment

This can be run by just executing VIPStatus.py, however using gunicorn or an equivalent is highly recommended for live environments.

## Built With

* [Quart](https://gitlab.com/pgjones/quart) - The Python web framework used
* [Cyborg](https://bootswatch.com/cyborg) - Bootstrap UI template
* [BootstrapCover](https://getbootstrap.com/docs/4.1/examples/cover/) - Cover template for Bootstrap by [@mdo] (https://twitter.com/mdo)
* [jQuery playSound](https://github.com/admsev/jquery-play-sound) - jQuery plugin used for audio alerts
* [jQuery preloader](https://github.com/mpchelnikov/jquery.preloader) - jQuery plugin used to display the loading animation


## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/sohandm/KeepalivedVRRPStatusCheck/tags). 

## Authors

* **Sohan Muthukumara** - *Initial work* - [sohandm](https://gitlab.com/sohandm)

See also the list of [contributors](https://gitlab.com/sohandm/KeepalivedVRRPStatusCheck/graphs/master) who participated in this project.

## License

This project is licensed under the GPL v3 License - see the [LICENSE.md](LICENSE.md) file for details
